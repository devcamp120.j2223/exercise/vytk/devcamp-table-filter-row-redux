const initialState={
    taskNameInput:'',
    taskList:[]
}

const TaskEvent=(state=initialState,action)=>{
    switch(action.type){
        case "TASK_INPUT_CHANGE":
            return {
                taskNameInput:action.value,
                taskList:state.taskList
            }
        case 'ADD_TASK':
            return {
                taskNameInput:'',
                taskList:[...state.taskList,{
                    taskName:state.taskNameInput,
                    status:false
                }]
            }
        default:
            return state;
    }
}

export default TaskEvent;