import { Grid,TextField,Button, FormLabel, Box} from "@mui/material";
import { Container } from "@mui/system";
import { useSelector,useDispatch } from "react-redux";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
function Task (){
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
          backgroundColor: theme.palette.common.black,
          color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
          fontSize: 14,
        },
      }));
      
      const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
          backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));
    const dispatch=useDispatch();

    const {taskList,taskNameInput}= useSelector((reduxData)=>reduxData.taskReducer);
    console.log(taskList);
    console.log(taskNameInput);

    const inputChangeHandler=(event)=>{
        console.log(event.target.value);
        dispatch({
            type:'TASK_INPUT_CHANGE',
            value:event.target.value
        })
    }
    const addTaskList=()=>{
        dispatch({
            type:'ADD_TASK'
        })
    }
    return(
        <Container>
            <Box sx={{ p: 3, border: "1px solid grey" }} mt={4} mb={4}>
                <Grid container textAlign={'center'}>
                    <Grid xs={6} md={3} sm={12}  item textAlign={'center'} mt={2}>
                        <FormLabel ><b>Nhập nội dung dòng</b></FormLabel>
                    </Grid>
                    <Grid xs={6} md={6} sm={12}  item>
                        <TextField fullWidth variant="outlined" value={taskNameInput} onChange={inputChangeHandler}/>
                    </Grid>
                    <Grid xs={6} md={3} lg={3} sm={12} item textAlign={'center'} mt={1}>
                        <Button size="large"  variant='outlined' onClick={addTaskList}>Thêm</Button>
                    </Grid>
                </Grid>
            </Box>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table" >
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>STT</StyledTableCell>
                            <StyledTableCell >Nội dung</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {taskList.map((task,index)=>{
                        return <StyledTableRow key={index}>
                            <StyledTableCell>{index+1}</StyledTableCell>
                            <StyledTableCell>{task.taskName}</StyledTableCell>
                            </StyledTableRow>
                    })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    )
}

export default Task;